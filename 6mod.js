var fs = require('fs')
var path = require('path')

module.exports = function(dirname, ext, callback)
{
    fs.readdir(dirname, function(err, files)
    {
        if(err)
        {
            callback(err, null);
        }
        else
        {
            result = [];
            for(var i=0; i<files.length; i++)
            {
                if (path.extname(files[i]) == "."+ext)
                {
                    result.push(files[i]);
                }
            }
            callback(null, result)
        }
    })

}